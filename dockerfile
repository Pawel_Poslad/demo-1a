FROM nginx:alpine

COPY default.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/src/app

RUN apk add -u npm

COPY package*.json ./

RUN npm install

COPY . .

RUN chmod +x start.sh

CMD ["sh", "start.sh"]
